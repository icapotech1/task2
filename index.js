const express = require("express");

const app = express();
const PORT = 3000;

app.get("/download", function (req, res) {
	const directoryPath = __dirname + "/sample.txt";
	res.download(directoryPath, (err) => {
		if (err) {
			res.status(500).send({
				message: "Could not download the file. " + err,
			});
		}
	});
});

app.get("/", (req, res) => {
	return res.send("Hello");
});

app.listen(PORT, () => {
	console.log("Listening on Port, ", PORT);
});
